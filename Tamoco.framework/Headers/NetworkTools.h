//
//  NetworkTools.h
//  Tamoco
//
//  Created by Sašo Sečnjak on 05/05/16.
//  Copyright © 2016 Tamoco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkTools : NSObject

+ (NSString *)getIPAddress;

@end
