![logo.gif.png](https://bitbucket.org/repo/XX5xgGx/images/3404555888-logo.gif.png)

For full details of the SDK and it's capabilities and installation details, continue reading below. 

## Requirements
The Tamoco iOS Client SDK support iOS 8 and above.

## Installation

#### CocoaPod Installation

To install the SDK using CocoaPods,

Create your Podfile in your project root directory or run this command in the root directory of your project
```bash
    pod init
```
Your pod file should look something like this

XCode 11 Compatible version of the Tamoco SDK

```ruby
    platform :ios, '8.0'

    target 'TamocoTestApp' do
      # use_frameworks! must be left uncommented for the TamocoSDK to function and compile correctly
      use_frameworks!

      # SDK
           pod 'Tamoco', :git => 'https://bitbucket.org/tamoco/tamoco-ios-sdk.git', :tag => '1.6.2'

    end

```

XCode 10.2 Compatible version of the Tamoco SDK
```ruby
    platform :ios, '8.0'

       target 'TamocoTestApp' do
         # use_frameworks! must be left uncommented for the TamocoSDK to function and compile correctly
         use_frameworks!

         # SDK
             pod 'Tamoco', '~> 1.5.7'

       end
```

If you require a different version of the TamocoSDK without this API call CNCopyCurrentNetworkInfo then enter the line below into your Podfile.


Swift 5 XCode 10.2 Compatible version of the Tamoco SDK

```ruby
    platform :ios, '8.0'

   target 'TamocoTestApp' do
     # use_frameworks! must be left uncommented for the TamocoSDK to function and compile correctly
     use_frameworks!

     # SDK
         pod 'Tamoco', :git => 'https://bitbucket.org/tamoco/tamoco-ios-sdk.git', :tag => '1.5.7NoWifi'

   end

    
```

Next run this command

```bash
    pod install
```

This will install the SDK as a new framework in your Pods directory. Please note that at this time a binary framework is included which can be run on iOS devices and the Xcode Simulator.

#### Carthage Installation

To create your Cartfile in your project root directory run this command in the terminal

```bash
    touch Cartfile
```

Then follow the instructions below based on your requirements.

#### XCode 11 Compatible version of the Tamoco SDK

The Tamoco SDK also supports Carthage in your Cartfile add this link


```bash
github "getsentry/sentry-cocoa" "4.3.4"
binary "https://raw.githubusercontent.com/Tamoco/TamocoPackageManager/master/TamocoSDK.json" == 1.6.2
```

Finally in the termianl run this command

```bash
    carthage update
```

This will fetch the Tamoco SDK and all the dependencies.

The TamocoSDK and the Sentry dependancy will be found in "Builds". From the Build folder drag and drop the Tamoco.framework and Sentry.framework into Link Binary with Libraries.
When added to your App, make sure both the Tamoco.framework and Sentry.framework are both set to "Embed and Sign".
Add a Run Script in build phases add these lines under Input files

```bash
$(SRCROOT)/Carthage/Builds/iOS/Tamoco.framework
$(SRCROOT)/Carthage/Builds/iOS/Sentry.framework 
```
and these lines in your Output files
```bash
$(BUILT_PRODUCTS_DIR)/$(FRAMEWORKS_FOLDER_PATH)/Tamoco.framework
$(BUILT_PRODUCTS_DIR)/$(FRAMEWORKS_FOLDER_PATH)/Sentry.framework
```

You will also require when using Carthage to strip out the unwanted architechtures when archiving your app to submit to the App Store also add this script to your Build Phase

```
 bash
    APP_PATH="${TARGET_BUILD_DIR}/${WRAPPER_NAME}"

    # This script loops through the frameworks embedded in the application and
    # removes unused architectures.
    find "$APP_PATH" -name '*.framework' -type d | while read -r FRAMEWORK
    do
    FRAMEWORK_EXECUTABLE_NAME=$(defaults read "$FRAMEWORK/Info.plist" CFBundleExecutable)
    FRAMEWORK_EXECUTABLE_PATH="$FRAMEWORK/$FRAMEWORK_EXECUTABLE_NAME"
    echo "Executable is $FRAMEWORK_EXECUTABLE_PATH"

    EXTRACTED_ARCHS=()

    for ARCH in $ARCHS
    do
    echo "Extracting $ARCH from $FRAMEWORK_EXECUTABLE_NAME"
    lipo -extract "$ARCH" "$FRAMEWORK_EXECUTABLE_PATH" -o "$FRAMEWORK_EXECUTABLE_PATH-$ARCH"
    EXTRACTED_ARCHS+=("$FRAMEWORK_EXECUTABLE_PATH-$ARCH")
    done

    echo "Merging extracted architectures: ${ARCHS}"
    lipo -o "$FRAMEWORK_EXECUTABLE_PATH-merged" -create "${EXTRACTED_ARCHS[@]}"
    rm "${EXTRACTED_ARCHS[@]}"

    echo "Replacing original executable with thinned version"
    rm "$FRAMEWORK_EXECUTABLE_PATH"
    mv "$FRAMEWORK_EXECUTABLE_PATH-merged" "$FRAMEWORK_EXECUTABLE_PATH"

    done
```
Also make sure the "Run Script only when installing" is checked.

#### XCode 10.2 Compatible version of the Tamoco SDK
The TamocoSDK will be found in "Build". From the Build folder drag and drop the Tamoco.framework into Link Binary with Libraries.

The Tamoco SDK also supports Carthage in your Cartfile add this link
```bash
git "https://bitbucket.org/tamoco/tamoco-ios-sdk.git" == 1.5.7
```

In the terminal execute 

```bash
    carthage update
```

This will fetch the Tamoco SDK and all the dependencies.
The TamocoSDK  will be found in "Builds". From the Build folder drag and drop the Tamoco.framework  into Link Binary with Libraries.
Add a Run Script in build phases add this line under Input files

```bash
$(SRCROOT)/Carthage/Builds/iOS/Tamoco.framework
```
and this line in your Output files
```bash
$(BUILT_PRODUCTS_DIR)/$(FRAMEWORKS_FOLDER_PATH)/Tamoco.framework
```


## Project Configuration
Your app project will require the following configuration settings to function properly with the Tamoco SDK.

### Embedded content
If you are using Obj C project, then you need to go to Build Settings of your target and under "Build Options" set "Always Embed Swift Standard Libraries" to YES

### Location permission description
As a proximity SDK, the Tamoco client requires location always permissions. 

Add the NSLocationWhenInUseUsageDescription key and the NSLocationAlwaysAndWhenInUseUsageDescription key to your Info.plist file. (Xcode displays these keys as "Privacy - Location When In Use Usage Description" and "Privacy - Location Always and When In Use Usage Description" in the Info.plist editor.)

If your app supports iOS 10 and earlier, add the NSLocationAlwaysUsageDescription key to your Info.plist file. (Xcode displays this key as "Privacy - Location Always Usage Description" in the Info.plist editor.)


### Bluetooth permission description

Add NSBluetoothPeripheralUsageDescription  key to your Info.plist. (Xcode displays this key as "Bluetooth Peripheral Usage Description" in the Info.plist editor.) This is a requirement from Apple as of Spring 2019 when submitting apps to the App Store

The string entered here will appear within the permissions dialog that is presented to the user when the app requests location permissions. This description should provide a clear explanation and value for why the app requires location information.

As of iOS 13 the SDK will be required to use NSBluetoothAlwaysUsageDescription as part of the bluetooth permission. Failing to add this will result in a runtime exception. But this is only applicable to iOS 13 or greater.

### Background modes
As of Tamoco SDK Version 1.5.6 the SDK now supports foreground only location, if an particular app can only support 'When In Use' and has no solid use case for using location in the background for App Store approval. To allow foreground only leave 'Location updates' capability unchecked. 
As always the Tamoco SDK gets the best results when background location is enabled.

Go to yor target and select "Capabilities" then enable "Background Modes" and thick: 

- Location updates (Leave this unchecked if Foreground only location is required )
- Uses Bluetooth LE accessories
- Background fetch
- Access Wifi Information (For Wifi enabled versions of the SDK only)


## Client Integration
### Update your app delegate
The first step to configure and initialize the Tamoco Client SDK into your app is to modify your main AppDelegate files. While initializing Tamoco client, you need to provide proper API Key, API Secret and custom ID. To change any of this parameters in runtime, just re-instantiate the Tamoco client.

In your AppDelegate file, start by including the Tamoco module:

```swift
import Tamoco
```

Or in case of using Objective C
```objc
#import <Tamoco/Tamoco-Swift.h>
```

Then, add the following additional Tamoco property:

	var tamoco: Tamoco!

Next, implement SDK initialisation in method

```swift
func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool`

	//Set the Logging level
	TamocoLogger.level = .error
	//Set the API Keys
	tamoco = Tamoco(apiKey: "TEST-API-KEY", apiSecret: "YOUR-SECRET")
	// Custom ID is an optional setting and could be set in the following way:
	tamoco.customId = "CUSTOM_ID"
    // By default Tamoco listens for location within an accuracy of 100 metres, you can set this higher or lower in accordance with the CLLocation
    CLLocationAccuracy definition
    tamoco.regionMonitoringAccuracy = 100
    //Optionally set an object as the Tamoco delegate in order to recieve custom payloads on event triggers set up via the Tamoco web portal
    tamoco.delegate = <delegateObject: TamocoDelegate>
    // By default set to true, ONLY set this property to false if the SDK is required to start at a different point in the app 
    tamoco.startSDKOnInitalize = false
    
```

The TamocoDelegate is defined with the following  protocol:

```swift
public protocol TamocoDelegate: class {
    func didReceiveJSON(payload: [String: Any])
}
```
### Start SDK On Initalize

startSDKOnInitalize is a custom property that gives control of when the SDK can be started, this custom property is set to true by default ONLY override this property and set to false when you require the SDK to be initailzed at another time. 

To start the SDK at a later time call the public function 'startSDK()'

Example:
```swift
    tamoco.startSDK()
```

### Starting and Stopping The SDK

If at any point the SDK is required to stop running after it has been initialized call the public function 'stopSDK()' this will stop the SDK monitoring any location updates, wifi information, geo fence information and beacon information. 

Example:
```swift
    tamoco.stopSDK()
```

To restart the SDK at any point call the public function 'startSDK()' this will start the SDK or initialise the SDK if the 'startSDKOnInitalize' property has been set to 'false'. 

Example:
```swift
    tamoco.startSDK()
```
### User Consented Data
If within your app you have your own Consent dialog that isn't iAB compliant then when a user grant's or revokes consent a UserDefault key needs to be set, depending on when the user grants or revokes their consent, to do this follow the next examples.

Example Granted Swift
```swift
    UserDefaults.standard.set(ConsentStatus.granted.rawValue, forKey: Tamoco_Consent)
```

Example Granted Objective C
```objc
     [NSUserDefaults.standardUserDefaults setInteger: ConsentStatusGranted forKey: self.tamoco.tamoco_Consent];
```

Example Revoke Swift
```swift
    UserDefaults.standard.set(ConsentStatus.revoked.rawValue, forKey: Tamoco_Consent)
```

Example Revoke Objective C
```objc
    [NSUserDefaults.standardUserDefaults setInteger: ConsentStatusRevoked forKey: self.tamoco.tamoco_Consent];
```

the above examples must ONLY be done if your app doesn't use an iAB Framework consent SDK.


### Custom ID
Custom ID is an optional setting related to sending additional tracking attribute e.g. application user ID.

### Debug messaging
For debug output messages, the level is needed to be set `TamocoLogger.level`. For all possible options, check `LogLevel` enum.

### Configure the Tamoco client object
Before initializing the Tamoco object you may custom configure a number of client settings. These settings allow you to balance the performance of the client relative to proximity responsiveness and battery consumption.

These settings are configured via an TamocoConfig.plist file included into the main bundle. Please note that if an entry is not defined, the default settings will apply.

For testing and development purposes, it is common to use aggressive values to see immediate results. However the default configuration values are the recommended production settings to optimize battery performance.

The following settings are available for configuration:

**ENABLE\_GEOFENCE\_RANGING:**  
This optional boolean property determines if the Tamoco geofence ranging functionality should be enabled (YES) or disabled (NO).

**ENABLE\_BEACON\_RANGING:**  
This optional boolean property determines if the Tamoco beacon ranging functionality should be enabled (YES) or disabled (NO).

**ENABLE\_WIFI\_RANGING:**  
This optional boolean property determines if the Tamoco WIFI ranging functionality should be enabled (YES) or disabled (NO).

**TRIGGERS\_UPDATE\_TIME:**
The minimum amount of time that needs to elapse between fetch operations.

**REGION\_MONITORING\_ACCURACY:**  
This is set to `desiredAccuracy` of the `CLLocationManager` object for Geofence and Beacon ranging.

**WIFI\_MONITORING\_ACCURACY:**  
This is set to `desiredAccuracy` of the `CLLocationManager` object for WiFi ranging. For WiFi ranging the `didUpdateLocations` is used, to check for the current WiFi.

**DELEGATE\_NOTIFICATIONS:**  
This optional property determines if the Tamoco triggers the callbacks upon trigger actions to the designated closures.

### Check for communication errors
The Tamoco exposes the closure `onCommunicationError: ((error: NSError) -> Void)?`, which is called if any communication error occurs.

## Action closures
The first method is to trigger proximity closure. You can set onCustomBeaconAction, onCustomFenceAction, onCustomWIFIAction closures, regarding to which actions you want to listen on. You can enable/disable triggering these closures by setting DELEGATE_NOTIFICATIONS property in configuration (defaults to YES).

## Library properties
These are library class properties that can be used to access inventory returned from backend service. Check below snippet how to use it:
```swift
let allBeacons: Set<BeaconTrigger>? = tamoco.beacons
```

### beacons: Set\<BeaconTrigger>?
All beacons that were returned from server. To listen for updates, start listen for 'TamocoOnTriggersFetched' on NSNotificationCenter.defaultCenter().

### geofences: Set\<GeoFenceTrigger>?
All GeoFences that were returned from server. To listen for updates, start listen for 'TamocoOnTriggersFetched' on NSNotificationCenter.defaultCenter().

### wifis: Set\<WifiTrigger>?
All WiFis that were returned from server. To listen for updates, start listen for 'TamocoOnTriggersFetched' on NSNotificationCenter.defaultCenter().

### currentBeacons: Set\<BeaconTrigger>?
Current beacons that are in range.

### currentGeofences: Set\<GeoFenceTrigger>?
Current GeoFences that are in range.

### currentWifis: Set\<WifiTrigger>?
Current WiFis that are in range.


## Vault service
You are able to send keys to the Vault service. The Vault stores client encrypted keys with a Device ID (IDFA/AAID). Before pushing to the Vault, SDK validates the key to meet server requirements. Key must be a 40-character (HEX) string (20 byte).

You can use Vault via vault property of the Tamoco SDK instance:
``` swift
	tamoco.vault.pushKey(“abcdef1234...”) { (exception) in {
	}
```

Or in case of Objective C:

```objc
	[self.tamoco.vault pushKey:@"abcdef1234..." finished:^(NSInteger exception, NSString \* \_Nullable key) {  
	}];
```

VaultError is enum which is returned as parameter on callback. For Objective c this enum is splitted to two parts, an integer and String which is the key that was used.

VaultError:

- `NoError` (Objective C integer value 0)
    - No error occurred and push was successful
- `NonValidFormat` (Objective C integer value 1)
    - Key is not in HEX format
- `NonValidLength` (Objective C integer value 2)
    - Key length is not 40-characters
- `NonValidDataLength` (Objective C integer value 3)
    - Key length after converting to bytes it is not 20-bytes
- `CantConvertToData` (Objective C integer value 4)
    - Key can not be converted to bytes
- `PushFailed` (Objective C integer value 5)
    - Pushing to the server has failed



## Tracking service
You are able to use this for any kind of additional tracking needed.

You can use Tracking via track property of the Tamoco SDK instance:
```swift
	tamoco.track.tap("code", variant: "qr") { (isError) in
	}
```

Or in case of Objective C:
```
	[self.tamoco.track tap:@"code" variant:@"qr" completion:^(BOOL) {
	}];
```

## Limitations
- WiFi scanning and DWELL is not reliable, due to the Apple strict policy about background running apps
- For reporting BleHover, more power is consumed because we need to start ranging beacon for this matter

# Change Log

This can be found [here](CHANGELOG.MD) file.
