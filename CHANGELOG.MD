![logo.gif.png](https://bitbucket.org/repo/XX5xgGx/images/3404555888-logo.gif.png)

Tamoco is a mobile SDK for regioning devices regarding to the specified triggers on the Tamoco backend.

## Change Log

### 1.6.2 16th September (SDK Size 4.8 MB)
- Improved publisher integration methods. 

### 1.6.1 16th September (SDK Size 4.8 MB)
- Fixed missing dependancy in Podspec
- Added XCode 11 support and iOS 13 support
- Migrated to new SDK crash and performance monitor Sentry. (Please check the documentation for more information)


### 1.5.7 28th August (SDK Size 6.6 MB)
- Fixed CoreData concurrency issue.

### 1.5.6 31st July (SDK Size 6.6 MB)
- Removed the backgroundLocationUpdates property (Please look at integration docs regarding the changes).
- Fixed issue that caused nil insertion on SDK internal database.


### 1.5.5 2nd July 2019 (SDK Size 6.6 MB)
- Fixed Podspec issue when importing Tamoco into project causing "No Module Found" issue

### 1.5.4 28th June 2019 (SDK Size 6.6 MB)
- Bug & Crash fixes

### 1.5.3 19th June 2019 (SDK Size 6.6 MB)
- Added iAb consent string or if the data has been consented via another method to each data point (see documentation)
- Improved threading and queuing
- Bug fixes

### 1.5.2 20th May 2019 (SDK Size 6.6 MB)
- Added new visit implementation, SDK can now detected visit's when geofences are configured to allow visits in the backend.
- Speed performance improvments
- Bug fixes
- Reduced size of SDK package.
- Adding SDK size to all future releases in the changelog, this is based on when the SDK is integrated in an app through the app store or testflght.

### 1.5.1 (Swift 5) 15th April 2019
- Fixed Objective C compatibility when compiling in XCode 10.2 using a simulator.

### 1.4.10 and 1.5.0 (Swift 5) 8th April 2019
- Added new public functions startSDK() and stopSDK() (See documentation for details)
- Added new startSDKOnInitalize custom property (See documentation for details)
- Added support for Swift 5 and XCode 10.2 (See documetation for details on which version to use depending on the version of XCode)
- Migrated to SQLite for geofence persistant storeage 
- Performance improvements.

### 1.4.9 14th March 2019
- Fixed issue with integrating SDK when using Objective C
- Update to the motion manager to perform less motion updates on the device improving overall performance of the SDK.
- Rollbar will automatically create publisher environments for crash logging.
- Other bug fixes.

### 1.4.7 11th February 2019
- Added a new improved geo fence enter exit logic when the detected motion type is walking. This should improve accuracy on geo fences with 150m radius or less.
- Found some closures had potential retain cycles these have now been fixed with the use of a weak reference. (Bug fix).
- Several performance improvements.

### 1.4.6 Release (XCode 10 support), 1.3.8(XCode 9 and below) 7th December
- Spec fix

### 1.4.5 Release (XCode 10 support), 1.3.7(XCode 9 and below) 7th December
- Bug fixes

### 1.4.4 Release (XCode 10 support), 1.3.5(XCode 9 and below) 30th November
- Rollbar global trigger removed now filtering stack trace for references to Tamoco.
- Fixed merge policy from Persistant Storage detected by Rollbar.

### 1.4.3 Release (XCode 10 support), 1.3.5(XCode 9 and below support) 29th November
 - New persistent storage for GeoFences.
 - Removal on all push notification releated code, (See updated docs for changes)
 - Removed listenOnly property from SDK. (No popup or local notifications will be triggered).
 - Increased SDK wake up triggers, but kept battery impact the same as previous version.
 - Bug fixes.
 - New WifiConnectDwell enum value added.

### 1.4.2 (XCode 10), 1.3.4 (XCode 9) Release 9th November 2018 
- Bug Fixes
- Removed dependancy

### 1.4.1 Release - 07 November 2018
- Fixed issue in public header for Rollbar dependancy.

### 1.4.0 Release (XCode 10), 1.3.2 Release (XCode 9) - 24 October 2018
- Support for Xcode 10
- Improvements to performance.
- Update to location precision.
- Bug Fixes.

### 1.3.1 Release - 14 May 2018

- Spec fix

### 1.3 Release - 14 May 2018

- Enable Bitcode for Apps integrating the SDK.  The Tamoco SDK is now built with bitcode enabled so integrating apps can enable this feature in order to decrease app install sizes.

### 1.2.1 Release - 15 April 2018

Improvements to location scanning to minimise background process utilisation and battery drain:

  - Streamlined server communication to batch updates to avoid continuous network usage
  - Remove timer based location didUpdateLocations
  - Decrease SDK size

### 1.1.0 Release - 8 December 2017

New Settings to direct the SDK operations more precisely:

  - Foreground Only Mode, to avoid background location updates for those apps not generally using location services themselves.
  - Register for specific messages delivered to the app.
  - Additional settings that can be applied dynamically
  - Disable location tracking when device battery is extremely low

### 1.0.4 Release - 16 October 2017

Bug Fixes

### 1.0.3 Release - 5 October 2017

- Configuration Fixes

### 1.0.2 Release - 27 July 2017

- Integer Overflow crash fix

### 1.0.1 Release - 7 July 2017

- Added new 'Listen Only Mode' parameter to disable notification and view popups generated from the Tamoco SDK

### 1.0.0 Release - 28 June

- Initial Release
